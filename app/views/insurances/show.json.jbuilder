json.extract! @insurance, :id, :insurance_id, :insurance_name, :contact_information, :address, :city, :state, :zip, :patient_id, :employee_id, :created_at, :updated_at
