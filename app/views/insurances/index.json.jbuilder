json.array!(@insurances) do |insurance|
  json.extract! insurance, :id, :insurance_id, :insurance_name, :contact_information, :address, :city, :state, :zip, :patient_id, :employee_id
  json.url insurance_url(insurance, format: :json)
end
