json.array!(@employees) do |employee|
  json.extract! employee, :id, :employee_id, :first_name, :last_name, :phone, :email, :dob, :sex, :address, :city, :state, :zip, :emergency_contact, :relation, :emergency_contact_number, :insurance_id
  json.url employee_url(employee, format: :json)
end
