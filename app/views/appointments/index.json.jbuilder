json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :physician_id, :patient_id, :date, :reason, :diagnostic_id, :employee_id
  json.url appointment_url(appointment, format: :json)
end
