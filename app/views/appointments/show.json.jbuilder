json.extract! @appointment, :id, :physician_id, :patient_id, :date, :reason, :diagnostic_id, :employee_id, :created_at, :updated_at
