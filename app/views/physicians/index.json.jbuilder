json.array!(@physicians) do |physician|
  json.extract! physician, :id, :employee_id, :first_name, :last_name, :phone, :email
  json.url physician_url(physician, format: :json)
end
