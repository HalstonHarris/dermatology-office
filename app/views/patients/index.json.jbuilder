json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :last_name, :phone, :email, :dob, :sex, :address, :city, :state, :zip, :emergency_contact, :relation, :emergency_contact_number, :insurance_id
  json.url patient_url(patient, format: :json)
end
