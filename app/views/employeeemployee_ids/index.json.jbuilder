json.array!(@employeeemployee_ids) do |employeeemployee_id|
  json.extract! employeeemployee_id, :id, :first_name, :last_name, :phone, :email, :dob, :sex, :address, :city, :state, :zip, :emergency_contact, :relation, :emergency_contact_number, :insurance_id
  json.url employeeemployee_id_url(employeeemployee_id, format: :json)
end
