json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :invoice_id, :date, :appointment_id, :patient_id, :patient_name, :insurance_id, :insurance_name, :fee, :Employee_id, :employee_name
  json.url invoice_url(invoice, format: :json)
end
