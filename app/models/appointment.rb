class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :employee
  belongs_to :diagnostic_code


end
