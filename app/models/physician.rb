class Physician < ActiveRecord::Base

  belongs_to :employee
  has_many :appointments
  has_many :patients, :through => :appointments

end
