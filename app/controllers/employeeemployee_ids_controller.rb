class EmployeeemployeeIdsController < ApplicationController
  before_action :set_employeeemployee_id, only: [:show, :edit, :update, :destroy]

  # GET /employeeemployee_ids
  # GET /employeeemployee_ids.json
  def index
    @employeeemployee_ids = EmployeeemployeeId.all
  end

  # GET /employeeemployee_ids/1
  # GET /employeeemployee_ids/1.json
  def show
  end

  # GET /employeeemployee_ids/new
  def new
    @employeeemployee_id = EmployeeemployeeId.new
  end

  # GET /employeeemployee_ids/1/edit
  def edit
  end

  # POST /employeeemployee_ids
  # POST /employeeemployee_ids.json
  def create
    @employeeemployee_id = EmployeeemployeeId.new(employeeemployee_id_params)

    respond_to do |format|
      if @employeeemployee_id.save
        format.html { redirect_to @employeeemployee_id, notice: 'Employeeemployee was successfully created.' }
        format.json { render :show, status: :created, location: @employeeemployee_id }
      else
        format.html { render :new }
        format.json { render json: @employeeemployee_id.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employeeemployee_ids/1
  # PATCH/PUT /employeeemployee_ids/1.json
  def update
    respond_to do |format|
      if @employeeemployee_id.update(employeeemployee_id_params)
        format.html { redirect_to @employeeemployee_id, notice: 'Employeeemployee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employeeemployee_id }
      else
        format.html { render :edit }
        format.json { render json: @employeeemployee_id.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employeeemployee_ids/1
  # DELETE /employeeemployee_ids/1.json
  def destroy
    @employeeemployee_id.destroy
    respond_to do |format|
      format.html { redirect_to employeeemployee_ids_url, notice: 'Employeeemployee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employeeemployee_id
      @employeeemployee_id = EmployeeemployeeId.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employeeemployee_id_params
      params.require(:employeeemployee_id).permit(:first_name, :last_name, :phone, :email, :dob, :sex, :address, :city, :state, :zip, :emergency_contact, :relation, :emergency_contact_number, :insurance_id)
    end
end
