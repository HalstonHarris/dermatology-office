require 'test_helper'

class EmployeeemployeeIdsControllerTest < ActionController::TestCase
  setup do
    @employeeemployee_id = employeeemployee_ids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employeeemployee_ids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employeeemployee_id" do
    assert_difference('EmployeeemployeeId.count') do
      post :create, employeeemployee_id: { address: @employeeemployee_id.address, city: @employeeemployee_id.city, dob: @employeeemployee_id.dob, email: @employeeemployee_id.email, emergency_contact: @employeeemployee_id.emergency_contact, emergency_contact_number: @employeeemployee_id.emergency_contact_number, first_name: @employeeemployee_id.first_name, insurance_id: @employeeemployee_id.insurance_id, last_name: @employeeemployee_id.last_name, phone: @employeeemployee_id.phone, relation: @employeeemployee_id.relation, sex: @employeeemployee_id.sex, state: @employeeemployee_id.state, zip: @employeeemployee_id.zip }
    end

    assert_redirected_to employeeemployee_id_path(assigns(:employeeemployee_id))
  end

  test "should show employeeemployee_id" do
    get :show, id: @employeeemployee_id
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employeeemployee_id
    assert_response :success
  end

  test "should update employeeemployee_id" do
    patch :update, id: @employeeemployee_id, employeeemployee_id: { address: @employeeemployee_id.address, city: @employeeemployee_id.city, dob: @employeeemployee_id.dob, email: @employeeemployee_id.email, emergency_contact: @employeeemployee_id.emergency_contact, emergency_contact_number: @employeeemployee_id.emergency_contact_number, first_name: @employeeemployee_id.first_name, insurance_id: @employeeemployee_id.insurance_id, last_name: @employeeemployee_id.last_name, phone: @employeeemployee_id.phone, relation: @employeeemployee_id.relation, sex: @employeeemployee_id.sex, state: @employeeemployee_id.state, zip: @employeeemployee_id.zip }
    assert_redirected_to employeeemployee_id_path(assigns(:employeeemployee_id))
  end

  test "should destroy employeeemployee_id" do
    assert_difference('EmployeeemployeeId.count', -1) do
      delete :destroy, id: @employeeemployee_id
    end

    assert_redirected_to employeeemployee_ids_path
  end
end
