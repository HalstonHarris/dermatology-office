require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post :create, employee: { address: @employee.address, city: @employee.city, dob: @employee.dob, email: @employee.email, emergency_contact: @employee.emergency_contact, emergency_contact_number: @employee.emergency_contact_number, employee_id: @employee.employee_id, first_name: @employee.first_name, insurance_id: @employee.insurance_id, last_name: @employee.last_name, phone: @employee.phone, relation: @employee.relation, sex: @employee.sex, state: @employee.state, zip: @employee.zip }
    end

    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee
    assert_response :success
  end

  test "should update employee" do
    patch :update, id: @employee, employee: { address: @employee.address, city: @employee.city, dob: @employee.dob, email: @employee.email, emergency_contact: @employee.emergency_contact, emergency_contact_number: @employee.emergency_contact_number, employee_id: @employee.employee_id, first_name: @employee.first_name, insurance_id: @employee.insurance_id, last_name: @employee.last_name, phone: @employee.phone, relation: @employee.relation, sex: @employee.sex, state: @employee.state, zip: @employee.zip }
    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_redirected_to employees_path
  end
end
