require 'test_helper'

class InsurancesControllerTest < ActionController::TestCase
  setup do
    @insurance = insurances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:insurances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create insurance" do
    assert_difference('Insurance.count') do
      post :create, insurance: { address: @insurance.address, city: @insurance.city, contact_information: @insurance.contact_information, employee_id: @insurance.employee_id, insurance_id: @insurance.insurance_id, insurance_name: @insurance.insurance_name, patient_id: @insurance.patient_id, state: @insurance.state, zip: @insurance.zip }
    end

    assert_redirected_to insurance_path(assigns(:insurance))
  end

  test "should show insurance" do
    get :show, id: @insurance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @insurance
    assert_response :success
  end

  test "should update insurance" do
    patch :update, id: @insurance, insurance: { address: @insurance.address, city: @insurance.city, contact_information: @insurance.contact_information, employee_id: @insurance.employee_id, insurance_id: @insurance.insurance_id, insurance_name: @insurance.insurance_name, patient_id: @insurance.patient_id, state: @insurance.state, zip: @insurance.zip }
    assert_redirected_to insurance_path(assigns(:insurance))
  end

  test "should destroy insurance" do
    assert_difference('Insurance.count', -1) do
      delete :destroy, id: @insurance
    end

    assert_redirected_to insurances_path
  end
end
