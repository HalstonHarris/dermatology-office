class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :dob
      t.string :sex
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :emergency_contact
      t.string :relation
      t.string :emergency_contact_number
      t.integer :insurance_id

      t.timestamps
    end
  end
end
