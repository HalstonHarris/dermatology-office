class CreateInsurances < ActiveRecord::Migration
  def change
    create_table :insurances do |t|
      t.integer :insurance_id
      t.string :insurance_name
      t.string :contact_information
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.integer :patient_id
      t.integer :employee_id

      t.timestamps
    end
  end
end
