class RemoveInsurances < ActiveRecord::Migration
  def change
    remove_column :insurances, :patient_id, :integer
    remove_column :insurances, :employee_id, :integer
  end
end
