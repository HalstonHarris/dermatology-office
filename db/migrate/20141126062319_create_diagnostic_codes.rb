class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :diagnostic_code
      t.text :description
      t.float :fee

      t.timestamps
    end
  end
end
