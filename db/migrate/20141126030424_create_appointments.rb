class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :appointment_id
      t.string :patient
      t.string :physician
      t.timestamp :appointment_time
      t.integer :diagnostic_code
      t.text :notes

      t.timestamps
    end
  end
end
