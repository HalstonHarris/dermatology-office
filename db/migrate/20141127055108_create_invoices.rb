class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :invoice_id
      t.date :date
      t.integer :appointment_id
      t.integer :patient_id
      t.string :patient_name
      t.integer :insurance_id
      t.string :insurance_name
      t.float :fee
      t.integer :Employee_id
      t.string :employee_name

      t.timestamps
    end
  end
end
