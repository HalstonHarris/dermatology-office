# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

DiagnosticCode.delete_all
open("/vagrant/projects/dermatologyoffice/diagnostic.csv") do |code|
  code.read.each_line do |code|
    diagnostic_code, description, fee = code.chomp.split(",")
    DiagnosticCode.create!( diagnostic_code: diagnostic_code, description: description, fee: fee)
  end
end